# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Efremov <yur.arh@gmail.com>, 2012, 2013.
# Alexander Lakhin <exclusion@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 20:16+0100\n"
"PO-Revision-Date: 2013-09-28 19:22+0400\n"
"Last-Translator: Alexander Lakhin <exclusion@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. i18n: ectx: property (title), widget (QGroupBox, authGroupBox)
#: rakia-advanced-options-widget.ui:17
#, kde-format
msgid "Authentication"
msgstr "Аутентификация"

#. i18n: ectx: property (text), widget (QLabel, authUserLabel)
#: rakia-advanced-options-widget.ui:26
#, kde-format
msgid "User:"
msgstr "Пользователь:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, authUserLineEdit)
#: rakia-advanced-options-widget.ui:33
#, kde-format
msgid ""
"Username for your SIP account. If your username is different from the user "
"part your SIP URI, provide it here."
msgstr ""
"Имя пользователя в вашей учётной записи SIP. Если ваше имя не совпадает с "
"именем в пользовательской части вашего адреса SIP, то вам следует указать "
"его в этом поле."

#. i18n: ectx: property (text), widget (QLabel, registrarLabel)
#: rakia-advanced-options-widget.ui:40
#, kde-format
msgid "Registrar:"
msgstr "Регистратор:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, registrarLineEdit)
#: rakia-advanced-options-widget.ui:47
#, kde-format
msgid ""
"Registrar for your SIP account. If your registrar is different from the "
"server part your SIP URI, provide it here."
msgstr ""
"Регистратор в вашей учётной записи SIP. Если регистратор не совпадает с "
"регистратором в служебной части вашего адреса SIP, то вам следует указать "
"его в этом поле."

#. i18n: ectx: property (title), widget (QGroupBox, proxyGroupBox)
#: rakia-advanced-options-widget.ui:57
#, kde-format
msgid "Proxy"
msgstr "Прокси"

#. i18n: ectx: property (text), widget (QLabel, proxyLabel)
#: rakia-advanced-options-widget.ui:66
#, kde-format
msgid "Proxy host:"
msgstr "Хост прокси:"

#. i18n: ectx: property (text), widget (QLabel, portLabel)
#: rakia-advanced-options-widget.ui:76
#, kde-format
msgid "Proxy port:"
msgstr "Порт прокси:"

#. i18n: ectx: property (text), widget (QLabel, transportLabel)
#: rakia-advanced-options-widget.ui:83
#, kde-format
msgid "Transport:"
msgstr "Транспорт:"

#. i18n: ectx: property (text), item, widget (KComboBox, transportComboBox)
#. i18n: ectx: property (text), item, widget (KComboBox, keepaliveMechanismComboBox)
#: rakia-advanced-options-widget.ui:91 rakia-advanced-options-widget.ui:246
#, kde-format
msgid "auto"
msgstr "авто"

#. i18n: ectx: property (text), item, widget (KComboBox, transportComboBox)
#: rakia-advanced-options-widget.ui:96
#, kde-format
msgid "udp"
msgstr "UDP"

#. i18n: ectx: property (text), item, widget (KComboBox, transportComboBox)
#: rakia-advanced-options-widget.ui:101
#, kde-format
msgid "tcp"
msgstr "TCP"

#. i18n: ectx: property (text), item, widget (KComboBox, transportComboBox)
#: rakia-advanced-options-widget.ui:106
#, kde-format
msgid "tls"
msgstr "TLS"

#. i18n: ectx: property (title), widget (QGroupBox, stunGroupBox)
#: rakia-advanced-options-widget.ui:124
#, kde-format
msgid "Custom STUN settings"
msgstr "Другие параметры STUN"

#. i18n: ectx: property (text), widget (QLabel, stunServerLabel)
#: rakia-advanced-options-widget.ui:136
#, kde-format
msgid "STUN server:"
msgstr "Сервер STUN:"

#. i18n: ectx: property (text), widget (QLabel, stunPortLabel)
#: rakia-advanced-options-widget.ui:143
#, kde-format
msgid "STUN port:"
msgstr "Порт STUN:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, stunServerLineEdit)
#: rakia-advanced-options-widget.ui:157
#, kde-format
msgid "STUN server address (FQDN or IP address)"
msgstr "Адрес сервера STUN (FQDN или IP-адрес):"

#. i18n: ectx: property (title), widget (QGroupBox, networkingGroupBox)
#: rakia-advanced-options-widget.ui:167
#, kde-format
msgid "Network and Routing"
msgstr "Сеть и маршрутизация"

#. i18n: ectx: property (toolTip), widget (QCheckBox, discoverBindingCheckBox)
#: rakia-advanced-options-widget.ui:173
#, kde-format
msgid "Enable discovery of public IP address beyond NAT"
msgstr "Включить обнаружение публичных IP-адресов за пределами NAT"

#. i18n: ectx: property (text), widget (QCheckBox, discoverBindingCheckBox)
#: rakia-advanced-options-widget.ui:176
#, kde-format
msgid "Discover Public Contact"
msgstr "Обнаружение публичных контактов"

#. i18n: ectx: property (toolTip), widget (QCheckBox, looseRoutingCheckBox)
#: rakia-advanced-options-widget.ui:183
#, kde-format
msgid "Enable loose routing as per RFC 3261"
msgstr "Включить свободную маршрутизацию, согласно RFC 3261"

#. i18n: ectx: property (text), widget (QCheckBox, looseRoutingCheckBox)
#: rakia-advanced-options-widget.ui:186
#, kde-format
msgid "Use Loose Routing"
msgstr "Использовать свободную маршрутизацию"

#. i18n: ectx: property (text), widget (QLabel, looseRoutingLabel)
#: rakia-advanced-options-widget.ui:193
#, kde-format
msgid "Routing:"
msgstr "Маршрут:"

#. i18n: ectx: property (text), widget (QLabel, discoverBindingLabel)
#: rakia-advanced-options-widget.ui:200
#, kde-format
msgid "NAT"
msgstr "NAT"

#. i18n: ectx: property (title), widget (QGroupBox, keepaliveGroupBox)
#: rakia-advanced-options-widget.ui:210
#, kde-format
msgid "KeepAlive"
msgstr "Отправка сигналов (Keepalive)"

#. i18n: ectx: property (text), widget (QLabel, keepaliveIntervalLabel)
#: rakia-advanced-options-widget.ui:216
#, kde-format
msgid "Interval:"
msgstr "Интервал:"

#. i18n: ectx: property (toolTip), widget (KIntNumInput, keepaliveIntervalNumInput)
#: rakia-advanced-options-widget.ui:223
#, kde-format
msgid ""
"Interval between keepalive probes in seconds (0 = disabled, unset = use a "
"default interval)"
msgstr ""
"Интервал отправки сигналов (Keepalive) в секундах (0 = отключено, не задан = "
"используется интервал по умолчанию)"

#. i18n: ectx: property (toolTip), widget (KComboBox, keepaliveMechanismComboBox)
#: rakia-advanced-options-widget.ui:242
#, kde-format
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:9pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Keepalive mechanism for "
"SIP registration</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">auto</span>: Keepalive management is up to the implementation</"
"p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">options</span>: Maintain registration with OPTIONS requests</"
"p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">register</span>: Maintain registration with REGISTER requests</"
"p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">stun</span>: Maintain registration with STUN as described in "
"IETF draft-sip-outbound</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">off</span>: Disable keepalive management</p></body></html>"
msgstr ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:9pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Механизм отправки сигналов "
"(Keepalive) для регистрации в SIP</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">авто</span>: keepalive выполняется в соответствии с "
"реализацией протокола</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">options</span>: поддерживать регистрацию с помощью запросов "
"OPTIONS.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">register</span>: поддерживать регистрацию с помощью запросов "
"REGISTER</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">stun</span>: поддерживать регистрацию с помощью STUN, согласно "
"спецификаций IETF.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">отключено</span>: отключить управление keepalive</p></body></"
"html>"

#. i18n: ectx: property (text), item, widget (KComboBox, keepaliveMechanismComboBox)
#: rakia-advanced-options-widget.ui:251
#, kde-format
msgid "register"
msgstr "register"

#. i18n: ectx: property (text), item, widget (KComboBox, keepaliveMechanismComboBox)
#: rakia-advanced-options-widget.ui:256
#, kde-format
msgid "options"
msgstr "options"

#. i18n: ectx: property (text), item, widget (KComboBox, keepaliveMechanismComboBox)
#: rakia-advanced-options-widget.ui:261
#, kde-format
msgid "stun"
msgstr "stun"

#. i18n: ectx: property (text), item, widget (KComboBox, keepaliveMechanismComboBox)
#: rakia-advanced-options-widget.ui:266
#, kde-format
msgid "off"
msgstr "выкл."

#. i18n: ectx: property (text), widget (QLabel, keepaliveMechanismLabel)
#: rakia-advanced-options-widget.ui:274
#, kde-format
msgid "Mechanism:"
msgstr "Механизм:"

#. i18n: ectx: property (windowTitle), widget (QWidget, RakiaMainOptionsWidget)
#: rakia-main-options-widget.ui:14
#, kde-format
msgid "Account Preferences"
msgstr "Параметры учётной записи"

#. i18n: ectx: property (text), widget (QLabel, accountLabel)
#: rakia-main-options-widget.ui:35
#, kde-format
msgid "SIP URI:"
msgstr "SIP URI:"

#. i18n: ectx: property (text), widget (QLabel, passwordLabel)
#: rakia-main-options-widget.ui:45
#, kde-format
msgid "Password:"
msgstr "Пароль:"

#. i18n: ectx: property (text), widget (QLabel, aliasLabel)
#: rakia-main-options-widget.ui:55
#, kde-format
msgid "Alias:"
msgstr "Псевдоним:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, aliasLineEdit)
#: rakia-main-options-widget.ui:62
#, kde-format
msgid ""
"This Alias is being used for SIMPLE, a instant messaging extension for SIP. "
"If left blank, your SIP URI will be shown to the receiver instead of your "
"Alias."
msgstr ""
"Псевдоним используется в SIMPLE, расширение для мгновенного обмена "
"сообщениями в SIP. Если не будет указано ни одного псевдонима, собеседнику "
"будет показан ваш адрес SIP."

#. i18n: ectx: property (text), widget (QLabel, label)
#: rakia-main-options-widget.ui:72
#, kde-format
msgid "<b>Example:</b> user@my.sip.server"
msgstr "<b>Например:</b> user@my.sip.server"
